terraform {
  required_providers {
    volterra = {
      source  = "volterraedge/volterra"
    }
    libvirt = {
      source = "dmacvicar/libvirt"
    }
  }
}

provider "volterra" {
  api_p12_file = var.api-creds-p12
  url          = var.api-url
}

provider "libvirt" {
  uri = "qemu:///system"
}

data "template_file" "kvm-ce-user_data" {
  count    = var.kvm-ce-nodes-number
  template = file("${path.module}/../../terraform/cloudinit/user-data.tpl")
  vars     = {
    "site-registration-token"             = var.site-registration-token
    "xc-environment-api-endpoint"         = var.xc-environment-api-endpoint
    "cluster-name"                        = var.kvm-ce-site-name
    "host-name"                           = "${var.kvm-ce-node-name}-${count.index}"
    "latitude"                            = var.kvm-ce-latitude
    "longitude"                           = var.kvm-ce-longitude
  }
}

data "template_file" "kvm-ce-meta_data" {
  count    = var.kvm-ce-nodes-number
  template = file("${path.module}/../../terraform/cloudinit/meta-data.tpl")
  vars     = {
    "host-name" = "${var.kvm-ce-node-name}-${count.index}"
  }
}

resource "libvirt_volume" "kvm-ce-volume" {
  count  = var.kvm-ce-nodes-number
  name   = "${var.kvm-ce-site-name}-${var.kvm-ce-node-name}-${count.index}.qcow2"
  pool   = var.kvm-ce-storage-pool
  source = var.kvm-ce-qcow2
  format = "qcow2"
}

resource "libvirt_cloudinit_disk" "kvm-ce-cloudinit" {
  count     = var.kvm-ce-nodes-number
  name      = "${var.kvm-ce-site-name}-${var.kvm-ce-node-name}-${count.index}-cloud-init.iso"
  pool      = var.kvm-ce-storage-pool
  user_data = element(data.template_file.kvm-ce-user_data.*.rendered, count.index)
  meta_data = element(data.template_file.kvm-ce-meta_data.*.rendered, count.index)
}

resource "libvirt_domain" "kvm-ce" {
  count  = var.kvm-ce-nodes-number
  name   = "${var.kvm-ce-site-name}-${var.kvm-ce-node-name}-${count.index}"
  memory = var.kvm-ce-memory
  vcpu   = var.kvm-ce-vcpu

  disk {
    volume_id = element(libvirt_volume.kvm-ce-volume.*.id, count.index)
  }

  cloudinit = element(libvirt_cloudinit_disk.kvm-ce-cloudinit.*.id, count.index)

  cpu {
    mode = "host-passthrough"
  }

  network_interface {
    network_name = "default"
  }

  network_interface {
    network_name = "inside"
  }

  console {
    type        = "pty"
    target_type = "serial"
    target_port = "0"
  }
}

resource "volterra_registration_approval" "kvm-ce-site-registration" {
  count        = var.kvm-ce-nodes-number
  depends_on   = [ libvirt_domain.kvm-ce ]

  cluster_name = var.kvm-ce-site-name
  hostname     = "${var.kvm-ce-node-name}-${count.index}"
  cluster_size = 3
  retry        = 5
  wait_time    = 60
}

resource "volterra_modify_site" "kvm-ce-site-assign-fleet" {
  depends_on = [ volterra_registration_approval.kvm-ce-site-registration ]

  name       = var.kvm-ce-site-name
  namespace  = "system"

  labels     = {
    "ves.io/fleet" = "kvm-ce-two-interfaces-fleet"
  }
}
