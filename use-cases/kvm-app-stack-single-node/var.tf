variable "kvm-app-stack-qcow2" {
  description = "KVM CE QCOW2 image source"
  default = "https://downloads.volterra.io/releases/images/2021-03-01/centos-7.2009.5-202103011045.qcow2"
}

variable "kvm-app-stack-memory" {
  description = "Memory allocated to KVM CE"
  default = "16384"
}

variable "kvm-app-stack-vcpu" {
  description = "Number of vCPUs allocated to KVM CE"
  default = "4"
}

variable "kvm-app-stack-cluster-name" {
  description = "KVM CE site/cluster name"
  default = "kvm-app-stack-single"
}

variable "kvm-app-stack-node-name" {
  description = "KVM CE node hostname"
  default = "master-0"
}

variable "kvm-app-stack-storage-pool" {
  description = "KVM CE storage pool name"
  default = "images"
}

variable "kvm-app-stack-latitude" {
  description = "KVM CE node latitude"
  default = "37.34"
}

variable "kvm-app-stack-longitude" {
  description = "KVM CE node longitude"
  default = "121.89"
}

variable "api-creds-p12" {
  description = "F5 XC P12 with API credentials"
}

variable "api-url" {
  description = "F5 XC API URL"
}

variable "pk8s-cluster-role-binding-subjects" {
  default = [""]
}

variable "site-registration-token" {
    description = "F5XC environment registration token"
}

variable "xc-environment-api-endpoint" {
    description = "F5XC environment Maurice API endpoint"
    default = "ves.volterra.io"
}
