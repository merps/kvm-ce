terraform {
  required_providers {
    volterra = {
      source  = "volterraedge/volterra"
    }
    libvirt = {
      source = "dmacvicar/libvirt"
    }
  }
}

provider "volterra" {
  api_p12_file = var.api-creds-p12
  url          = var.api-url
}

provider "libvirt" {
  uri = "qemu:///system"
}

data "volterra_namespace" "system" {
  name = "system"
}

resource "volterra_k8s_cluster_role" "allow_all" {
  name        = format("%s-allow-all", var.kvm-app-stack-cluster-name)
  namespace   = data.volterra_namespace.system.name
  description = "K8s cluster role with allow-all policy"

  policy_rule_list {
    policy_rule {
      resource_list {
        api_groups      = ["*"]
        resource_types  = ["*"]
        verbs           = ["*"]
      }
    }
  }
}

resource "volterra_k8s_cluster_role_binding" "admin_cluster_role_binding" {
  name        = format("%s-cluster-role-binding", var.kvm-app-stack-cluster-name)
  namespace   = data.volterra_namespace.system.name
  description = "K8s cluster role binding for all subjects (users)"

  k8s_cluster_role {
    name      = volterra_k8s_cluster_role.allow_all.name
    namespace = data.volterra_namespace.system.name
    tenant    = data.volterra_namespace.system.tenant_name
  }

  dynamic "subjects" {
    for_each = var.pk8s-cluster-role-binding-subjects
    content {
      user  = subjects.value
    }
  }
}

resource "volterra_k8s_cluster" "pk8s_cluster" {
  name        = format("%s-pk8s-cluster", var.kvm-app-stack-cluster-name)
  namespace   = data.volterra_namespace.system.name

  local_access_config {
    local_domain = "${var.kvm-app-stack-cluster-name}-pk8s-cluster.local"
    default_port = true
  }

  use_custom_cluster_role_list {
    cluster_roles {
      name      = volterra_k8s_cluster_role.allow_all.name
      namespace = data.volterra_namespace.system.name
      tenant    = data.volterra_namespace.system.tenant_name
    }

    cluster_roles {
      name      = "ves-io-admin-cluster-role"
      namespace = data.volterra_namespace.system.name
      tenant    = data.volterra_namespace.system.tenant_name
    }
  }

  use_custom_cluster_role_bindings {
    cluster_role_bindings {
      name      = volterra_k8s_cluster_role_binding.admin_cluster_role_binding.name
      namespace = data.volterra_namespace.system.name
      tenant    = data.volterra_namespace.system.tenant_name
    }

    cluster_role_bindings {
      name      = "ves-io-admin-cluster-role-binding"
      namespace = data.volterra_namespace.system.name
      tenant    = data.volterra_namespace.system.tenant_name
    }
  }

  global_access_enable        = true
  cluster_scoped_access_deny  = true
  no_cluster_wide_apps        = true
  no_insecure_registries      = true
  use_default_psp             = true
}

resource "volterra_voltstack_site" "pk8s_voltstack_site" {
  name                  = var.kvm-app-stack-cluster-name
  namespace             = data.volterra_namespace.system.name
  description           = "App Stack site for KVM environment"

  volterra_certified_hw = "kvm-regular-nic-voltmesh"
  master_nodes          = [ "${var.kvm-app-stack-node-name}" ]

  k8s_cluster {
    name      = volterra_k8s_cluster.pk8s_cluster.name
    namespace = data.volterra_namespace.system.name
    tenant    = data.volterra_namespace.system.tenant_name
  }

  lifecycle {
    ignore_changes = [labels]
  }

  sw {
    default_sw_version = true
  }

  os {
    default_os_version = true
  }

  no_bond_devices         = true
  disable_gpu             = true
  logs_streaming_disabled = true
  default_network_config  = true
  default_storage_config  = true
  deny_all_usb            = true
}

data "template_file" "kvm-app-stack-user_data" {
  template = file("${path.module}/../../terraform/cloudinit/user-data.tpl")
  vars = {
    "site-registration-token"             = var.site-registration-token
    "xc-environment-api-endpoint"         = var.xc-environment-api-endpoint
    "cluster-name"                        = var.kvm-app-stack-cluster-name
    "host-name"                           = var.kvm-app-stack-node-name
    "latitude"                            = var.kvm-app-stack-latitude
    "longitude"                           = var.kvm-app-stack-longitude
  }
}

data "template_file" "kvm-app-stack-meta_data" {
  template = file("${path.module}/../../terraform/cloudinit/meta-data.tpl")
  vars = {
    "host-name" = var.kvm-app-stack-node-name
  }
}

resource "libvirt_volume" "kvm-app-stack-volume" {
  name   = "${var.kvm-app-stack-cluster-name}-${var.kvm-app-stack-node-name}.qcow2"
  pool   = var.kvm-app-stack-storage-pool
  source = var.kvm-app-stack-qcow2
  format = "qcow2"
}

resource "libvirt_cloudinit_disk" "kvm-app-stack-cloudinit" {
  name      = "${var.kvm-app-stack-cluster-name}-${var.kvm-app-stack-node-name}-cloud-init.iso"
  pool      = var.kvm-app-stack-storage-pool
  user_data = data.template_file.kvm-app-stack-user_data.rendered
  meta_data = data.template_file.kvm-app-stack-meta_data.rendered
}

resource "libvirt_domain" "kvm-app-stack" {
  name   = "${var.kvm-app-stack-cluster-name}-${var.kvm-app-stack-node-name}"
  memory = var.kvm-app-stack-memory
  vcpu   = var.kvm-app-stack-vcpu

  disk {
    volume_id = libvirt_volume.kvm-app-stack-volume.id
  }

  cloudinit = libvirt_cloudinit_disk.kvm-app-stack-cloudinit.id

  cpu {
    mode = "host-passthrough"
  }

  network_interface {
    network_name = "default"
  }

  console {
    type        = "pty"
    target_type = "serial"
    target_port = "0"
  }
}

resource "volterra_registration_approval" "kvm-app-stack-node-registration" {
  depends_on   = [ libvirt_domain.kvm-app-stack ]

  cluster_name = var.kvm-app-stack-cluster-name
  hostname     = var.kvm-app-stack-node-name
  cluster_size = 1
  retry        = 5
  wait_time    = 60
}
